import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, Button, Alert, Pressable } from 'react-native';

export default function App() {
  return (
    <View style={styles.container}>
      <Text>So simple, it is all there for you</Text>
      <Pressable
        style={styles.button}
        onPress={() => Alert.alert('Yes really! The edits are just happening')}
      ><Text style={styles.text}>really?</Text></Pressable>
      <StatusBar style="auto" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  button: {
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: 12,
    paddingHorizontal: 32,
    borderRadius: 4,
    elevation: 3,
    backgroundColor: 'rgb(255,0,0)',
  },
  text: {
    fontSize: 16,
    lineHeight: 21,
    fontWeight: 'bold',
    letterSpacing: 0.25,
    color: 'rgb(0,255,0)',
  },
});
